function invert(obj) {
  if (typeof obj == "object" && obj != null) {
    let result = {};
    for (const key in obj) {
      if (typeof obj[key] == "object") {
        invert(obj[key]);
      }
      result = { ...result, [obj[key]]: key };
    }
    return result;
  }
}
module.exports = invert;

