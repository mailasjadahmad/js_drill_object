const mapObject = require("../map_for_object");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

console.log(
  mapObject(testObject, (element, key, obj) => {
    return element + " " + element;
  })
);
