function pairs(obj) {
  if (typeof obj == "object" && obj != null) {
    const result = [];
    for (const key in obj) {
      if (typeof obj[key] == "object") {
        pairs(obj[key]);
      } else {
        result.push([key, obj[key]]);
      }
    }
    return result;
  }
}
module.exports = pairs;


