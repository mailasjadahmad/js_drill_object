function defaults(obj, defaultProps) {
  if (typeof obj == "object" && obj != null) {
    for (const key in defaultProps) {
      obj[key] = defaultProps[key];
    }
    return obj;
  }
}

module.exports = defaults;
